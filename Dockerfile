FROM node:10.8.0-alpine AS build
ADD . /app
WORKDIR /app
RUN npm install && npm run build

FROM node:10.8.0-alpine
WORKDIR /app
RUN addgroup -S appgroup && \
    adduser -S appuser -G appgroup && \
    chown -R appuser:appgroup /app
USER appuser
COPY --from=build /app/package.json .
COPY --from=build /app/package-lock.json .
COPY --from=build /app/dist ./dist
RUN npm install --production

CMD ["npm", "start"]
