# Crawler

Crawls all website URLs

## Environment variables

1. `CRAWLER_LOG_LEVEL` (`error`): One of: `none`, `silly`, `debug`, `info`, `warn` (or `warning`), `error` or `critical`.
1. `CRAWLER_CONCURRENCY` (`5`): How many URLs will be crawled concurrently.
1. `CRAWLER_SEED_URL`: First URL to be crawled.

