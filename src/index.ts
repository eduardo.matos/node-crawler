import Crawler from './crawler';

async function main(): Promise<void> {
  const crawler = new Crawler(process.env.CRAWLER_SEED_URL);
  return crawler.run();
}

if (require.main === module) {
  main();
}
