import * as async from 'async';
import axios from 'axios';
import * as cheerio from 'cheerio';
import * as _ from 'lodash';
import { URL } from 'url';
import * as config from './config';
import logger from './logger';

interface IQueueTask {
  url: string;
}

export default class Crawler {
  private baseUrl: URL;

  private queue: async.AsyncQueue<IQueueTask>;

  private toVisit = new Set();
  private visited = new Set();

  private resolve: () => void;
  private reject: () => void;

  constructor(baseUrl: string) {
    this.baseUrl = new URL(baseUrl);
    this.toVisit.add(this.baseUrl.href);

    this.setupQueue();
  }

  public run(): Promise<void> {
    return new Promise((resolve, reject) => {
      this.resolve = resolve;
      this.reject = reject;
      this.enqueueLinksToVisit();
    });
  }

  public fetch(url: string): Promise<string> {
    return new Promise(async (resolve, reject) => {

      try {
        const response = await axios.get(url);
        logger.debug(`Crawled ${url}`);
        resolve(response.data);
      } catch (e) {
        reject();
      }
    });
  }

  public extractLinks(html: string): string[] {
    const $ = cheerio.load(html);
    const links = [];

    $('a').each((i, el) => {
      const href = $(el).attr('href');

      if (!href) {
        return;
      }

      try {
        if (href.startsWith('/') || href.startsWith('#')) {
          links.push(`${this.baseUrl.protocol}//${this.baseUrl.host}${href}`);
        } else if (this.baseUrl.host === new URL(href).host) {
          links.push(href);
        }
      } catch (e) {
        logger.error(`Could not parse href='${href}' as url`);
      }
    });

    return links;
  }

  protected onFinish() {
    this.resolve();
  }

  private setupQueue() {
    this.queue = async.queue(this.worker.bind(this), config.CONCURRENCY);
    this.queue.drain = this.onFinish.bind(this);

    this.setupQueueSizeLogging();
  }

  private setupQueueSizeLogging() {
    setInterval(() => {
      if (!this.queue.empty()) {
        logger.info(`Queue: ${this.queue.length()} :: Visited ${this.visited.size}`);
      }
    }, 2000);
  }

  private async worker(task: IQueueTask, next: () => void) {
    const html = await this.fetch(task.url);
    const links = _.uniq(this.extractLinks(html));

    this.markAsVisited(task.url);
    this.addToVisit(...links);
    this.enqueueLinksToVisit();

    next();
  }

  private markAsVisited(url: string): void {
    this.visited.add(url);
  }

  private addToVisit(...links: string[]): void {
    links.forEach((link) => {
      if (!this.visited.has(link) && !this.toVisit.has(link)) {
        this.toVisit.add(link);
      }
    });
  }

  private enqueueLinksToVisit(): void {
    logger.debug(`Added ${this.toVisit.size} URLs to visit`);
    this.queue.push(Array.from(this.toVisit).map((url) => {
      this.toVisit.delete(url);
      return { url };
    }));
  }
}
