import * as winston from 'winston';

export default winston.createLogger({
  format: winston.format.json(),
  level: process.env.CRAWLER_LOG_LEVEL || 'error',
  transports: [
    new winston.transports.Console(),
  ],
});
