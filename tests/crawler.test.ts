import { expect } from 'chai';
import axios from 'axios';
import * as sinon from 'sinon';
import './setup';
import Crawler from '../src/crawler';

describe('Crawler', () => {
  let crawler;
  let get;

  beforeEach(() => (get = sinon.stub(axios, 'get')));
  afterEach(() => get.restore());

  beforeEach(() => (crawler = new Crawler('http://site.com')));

  describe('fetch', () => {
    it('Fetches html', async () => {
      get.resolves({ data: `<a href="/foo"></a>` })

      const html = await crawler.fetch('site.com')
      expect(html).to.equal('<a href="/foo"></a>');
    });

    it('Raises error if page cant be fetched', async () => {
      get.rejects()
      expect(crawler.fetch('site.com')).to.eventually.rejected
    });
  });

  describe('extractLinks', () => {
    it('Gets all links', () => {
      const links = crawler.extractLinks('<a href="/foo"></a><a href="http://site.com/bar"></a>');
      expect(links).to.eql(['http://site.com/foo', 'http://site.com/bar']);
    });

    it('Ignores external links', () => {
      const links = crawler.extractLinks('<a href="http://external.com/baz"></a>');
      expect(links).to.eql([]);
    });

    it('Accepts links which starts with hash sign', () => {
      const links = crawler.extractLinks('<a href="#yay"></a>');
      expect(links).to.eql(['http://site.com#yay']);
    });
  });

  describe('run', () => {
    it('Visits base url and all links returned', async () => {
      get.resolves({ data: '' });
      get.onFirstCall().resolves({ data: '<a href="/foo"></a>' });
      await crawler.run();

      expect(get.callCount).to.equal(2);
    });

    it('Ignores external links', () => {
      const links = crawler.extractLinks('<a href="http://external.com/baz"></a>');
      expect(links).to.eql([]);
    });
  });
});

